<?php

use Wa72\HtmlPageDom\HtmlPage;
use JonnyW\PhantomJs\Client;

abstract class WebLinkSearch
{	
	protected $web_link;
	protected $curl_response;
	protected $page;
	protected $source;
	protected $html_tag;

	/**
	 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
	 * array containing the HTTP server response header fields and content.
	 */
	public function readWebPage($web_link)
	{
		$this->web_link = $web_link;

	    // // $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
	    // $user_agent='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36';
	    // // $user_agent='Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3';

	    // $options = array(
	    //     CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
	    //     CURLOPT_POST           =>false,        //set to GET
	    //     CURLOPT_USERAGENT      => $user_agent, //set user agent
	    //     CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
	    //     CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
	    //     CURLOPT_RETURNTRANSFER => true,     // return web page
	    //     CURLOPT_HEADER         => false,    // don't return headers
	    //     CURLOPT_FOLLOWLOCATION => true,     // follow redirects
	    //     CURLOPT_ENCODING       => "",       // handle all encodings
	    //     CURLOPT_AUTOREFERER    => true,     // set referer on redirect
	    //     CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
	    //     CURLOPT_TIMEOUT        => 120,      // timeout on response
	    //     CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
	    //     CURLOPT_SSL_VERIFYPEER => false,	// Prevent cURL from verifying SSL certificate
	    //     CURLOPT_FAILONERROR	   => true,		// Script should fail silently on error
	    //     CURLOPT_COOKIESESSION  => true,		// Use cookies
	    // );
	    
	    // $ch      = curl_init( $this->web_link );
	    // curl_setopt_array( $ch, $options );
	    // $content = curl_exec( $ch );
	    // $err     = curl_errno( $ch );
	    // $errmsg  = curl_error( $ch );
	    // $curl_response  = curl_getinfo( $ch );
	    // curl_close( $ch );

	    // $curl_response['errno']   = $err;
	    // $curl_response['errmsg']  = $errmsg;
	    // $curl_response['content'] = mb_convert_encoding($content, 'HTML-ENTITIES', "GB2312");

	    // if($err !== 0 && $errmsg !== 200)
	    // {
	    // 	die($errmsg);
	    // }
	    
	    // //file_put_contents('test-curl.html', $curl_response['content']);
	    // return $this->page = $curl_response['content'];
	     
	     
	     

    
		// $client = Client::getInstance();
		// $client->getEngine()->setPath(BASE_PATH . '/bin/phantomjs.exe');
		// $client->getEngine()->addOption('--ssl-protocol=any');
		// $client->getEngine()->addOption('--ignore-ssl-errors=yes');

		// $request  = $client->getMessageFactory()->createRequest();
		// $response = $client->getMessageFactory()->createResponse();

		// $request->setMethod('GET');
		// $request->setUrl($this->web_link);

		// $client->send($request, $response);

		// if($response->getStatus() === 200) {
		// 	//file_put_contents('test-phantomjs.html', $response->getContent());
		//     return $this->page = $response->getContent();
		// }
		// else{
		// 	die("Unable to connect to the site: " . $response->getStatus());
		// }




		$process = proc_open(
			BASE_PATH . '/bin/phantomjs webcontent.js ' . $this->web_link, 
			array(
                array('pipe', 'r'),
                array('pipe', 'w'),
                array('pipe', 'w')
            ),
            $pipes,
            null,
            null
        );

	    if (!is_resource($process)) die("Unable to connect to the site.");	
		
		$content = stream_get_contents($pipes[1]);		    
	    fclose($pipes[0]);
        fclose($pipes[1]);
        fclose($pipes[2]);
	    proc_close($process);

	    return $this->page = new HtmlPage($content);
	}

	protected function isJson($str)
	{
		try
		{
			if(trim($str) === '') return false;

			$result = json_decode($str);
			if(json_last_error() != JSON_ERROR_NONE || json_last_error() == JSON_ERROR_SYNTAX) return false;

		    return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}

	/**
	 * Read data from the DOM.
	 */
	public function crawlData()
	{
		return array(
			'source'	=> $this->source,
			'web_link'	=> $this->web_link,
			'title'		=> method_exists($this, 'getTitle') ? $this->getTitle() : '',
			'img'		=> method_exists($this, 'getImage') ? $this->getImage() : '',
			'price_rmb'	=> method_exists($this, 'getPrice') ? $this->getPrice() : '',
			'color'		=> method_exists($this, 'getColor') ? $this->getColor() : '',
			'size'		=> method_exists($this, 'getSize') ? $this->getSize() : '',
			'attribute'	=> method_exists($this, 'getAttribute') ? $this->getAttribute() : '',
		);
	}
}