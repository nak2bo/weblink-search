<?php 

require_once "WebLinkSearch.php";

use \Wa72\HtmlPageDom\HtmlPage;

class Tmall extends WebLinkSearch
{
	public function __construct($html_tag)
	{
		$this->source = 'tmall';	
		$this->html_tag = $html_tag;
	}

	protected function getTitle()
	{		
		foreach ($this->html_tag->title as $tag) {
			$node = $this->page->filter($tag);
			if($node->count === 1) return trim($node->first()->text());
		}
	}

	protected function getImage()
	{		
		foreach ($this->html_tag->image as $tag) {
			$node = $this->page->filter($tag);
			if($node->count === 1) return $node->extract('src')[0];
			elseif($node->count > 1) return $node->first()->extract('src')[0];
		}
	}

	protected function getPrice()
	{
		foreach ($this->html_tag->price as $tag) {
			$node = $this->page->filter($tag);
			if($node->count > 0) 
			{
				$node->children()->each(function ($node, $i) { $node->remove(); });
				return $node->first()->text();
			}
		}
	}

	protected function getColor()
	{
		foreach ($this->html_tag->color as $tag) 
		{
			$node = $this->page->filter($tag);

			if($node->count > 1)
			{
				$items = $node->extract('style');
				$array = array();

				foreach ($items as $item) 
				{
					
			    	array_push($array, substr($item, strlen('background:url('), strpos($item, ')') - strlen('background:url(')));					    					 			
				}	

				return $array;		    
			}
		}
	}

	protected function getSize()
	{
		foreach ($this->html_tag->size as $tag) {
			$node = $this->page->filter($tag);
			if($node->count === 1)
			{
				return $node->text();
			}
			elseif($node->count > 1)
			{
				$array = array();
				foreach ($node as $item) {
					array_push($array, $item->textContent);
				}				

				return $array;
			}
		}
	}
}