<?php

define('BASE_PATH', dirname(__FILE__));

require "vendor/autoload.php";
require "class/_1688.php";
require "class/Taobao.php";
require "class/Tmall.php";
require "class/Jingdong.php";

$html_tag_obj = json_decode(file_get_contents('html_tag.json'));

// $web_store = '1688';
// $web_store = 'taobao';
// $web_store = 'tmall';
$web_store = 'jingdong';

switch ($web_store) {
	case '1688':
		$web_link_search = new _1688($html_tag_obj->_1688);
		$web_link_search->readWebPage('https://detail.1688.com/offer/545064444714.html?spm=a26e3.8027625.1999173159.1.H7YryW');
		// $web_link_search->readWebPage('https://detail.1688.com/offer/548272075292.html?spm=a260d.8120639.j2bnz8nq.4.sHzBiR');		
		break;

	case 'taobao':
		$web_link_search = new Taobao($html_tag_obj->taobao);
		$web_link_search->readWebPage('https://world.taobao.com/item/17300238787.htm?fromSite=main&spm=a21bp.8294655.floor_seaFpFloorChild.11.JZH0fH&abtest=_AB-LR1382-PR1382&pos=8&abbucket=_AB-M1382_B6&bucket_tag=E-BUCKET&pid=58383&acm=2017011718.1003.1.1527601&scm=1003.1.2017011718.A-BUCKET_oVA7cShT_8_58383_17300238787_1527601');
		break;

	case 'tmall':
		$web_link_search = new Tmall($html_tag_obj->tmall);
		// $web_link_search->readWebPage('https://detail.tmall.com/item.htm?spm=a3211.0-7589174.userDefined_1494049579349_11.48.m27jJr&id=549988557890&gccpm=13717534.600.2.subject-1009&sta=gccpm:13717534.600.2.subject-1009&track_params={%22gccpm%22:%2213717534.600.2.subject-1009%22}');
		$web_link_search->readWebPage('https://world.tmall.com/item/546720190782.htm?spm=a222t.8063993.5375784148.12.U3szB8&acm=lb-zebra-164656-1010079.1003.4.862698&id=546720190782&scm=1003.4.lb-zebra-164656-1010079.ITEM_546720190782_862698&sku_properties=10004:385316259;5919063:6536025;12304035:3222911');
		// $web_link_search->readWebPage('https://detail.tmall.com/item.htm?spm=a3211.0-7589174.userDefined_1494049579349_11.43.m27jJr&id=549690867065&gccpm=13717534.600.2.subject-1009&sta=gccpm:13717534.600.2.subject-1009&track_params={%22gccpm%22:%2213717534.600.2.subject-1009%22}');
		// $web_link_search->readWebPage('https://content.tmall.com/wow/pegasus/subject/0/1627374189/7526111?spm=a220o.7142085.0.0.nGzh5d&abbucket=_AB-M220_B6&tailer=1&actId=13619719&acm=03224.1003.1.1061409&resId=35388&abtest=_AB-LR220-PR220&wh_header=1&pos=2&header=1&id=7526111&scm=1003.1.03224.ACTIVITY0_13619719-C1S1_1061409&gccpm=13619719.102.2.subject-1002.35388&wh_tailer=1');
		// $web_link_search->readWebPage('https://world.tmall.com/item/527394993875.htm?spm=a3211.0-7589174.userdefined_1494049579349_11.7.m27jjr&id=527394993875&gccpm=13717534.600.2.subject-1009&sta=gccpm:13717534.600.2.subject-1009&track_params={%22gccpm%22:%2213717534.600.2.subject-1009%22}&tbpm=3');
		// $web_link_search->readWebPage('https://detail.tmall.com/item.htm?spm=a3211.0-7589174.userDefined_1494049579349_11.13.m27jJr&id=531212617407&gccpm=13717534.600.2.subject-1009&sta=gccpm:13717534.600.2.subject-1009&track_params={%22gccpm%22:%2213717534.600.2.subject-1009%22}');
		// $web_link_search->readWebPage('https://world.tmall.com/item/531212617407.htm?spm=a3211.0-7589174.userDefined_1494049579349_11.13.m27jJr&id=531212617407&gccpm=13717534.600.2.subject-1009&sta=gccpm:13717534.600.2.subject-1009&track_params={%22gccpm%22:%2213717534.600.2.subject-1009%22}');
		// $web_link_search->readWebPage('https://world.tmall.com/item/549690867065.htm?spm=a3211.0-7589174.userDefined_1494049579349_11.43.m27jJr&id=549690867065&gccpm=13717534.600.2.subject-1009&sta=gccpm:13717534.600.2.subject-1009&track_params={%22gccpm%22:%2213717534.600.2.subject-1009%22}');
		break;

	case 'jingdong':
		$web_link_search = new Jingdong($html_tag_obj->jingdong);
		// $web_link_search->readWebPage('http://item.jd.com/4074575.html');
		$web_link_search->readWebPage('http://item.jd.com/4691178.html');
		break;
}

$product = $web_link_search->crawlData();

echo '<pre>', print_r($product), '</pre>';