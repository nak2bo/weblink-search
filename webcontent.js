var system = require('system');
var page = require('webpage').create();

if (system.args.length === 1) {
    console.log('Try to pass some args when invoking this script!');
} else if (system.args.length === 2) {
    page.open(system.args[1], function (status) {
		system.stdout.write(page.content);
		phantom.exit();
	});
}